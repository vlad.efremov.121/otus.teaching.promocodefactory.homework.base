using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRequest
    {
        [Required]
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        [Required]
        public string Email { get; set; }
        
        public List<Guid> RoleIds { get; set; }
        
        public int AppliedPromocodesCount { get; set; }
    }
}