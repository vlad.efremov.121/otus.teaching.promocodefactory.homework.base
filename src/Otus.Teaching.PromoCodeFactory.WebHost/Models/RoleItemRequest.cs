using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class RoleItemRequest
    {
        [Required]
        public string Name { get; set; }
        
        public string Description { get; set; }
    }
}